﻿namespace DrawApp
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.palletPanel = new System.Windows.Forms.Panel();
            this.moveBtn = new System.Windows.Forms.Button();
            this.penToolBtn = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btnColor = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAddLayer = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetSize = new System.Windows.Forms.Button();
            this.textPicHeight = new System.Windows.Forms.TextBox();
            this.textPicWidth = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.cmbWidth = new System.Windows.Forms.ComboBox();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.layerWrapper = new System.Windows.Forms.Panel();
            this.palletPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // palletPanel
            // 
            this.palletPanel.BackColor = System.Drawing.SystemColors.Info;
            this.palletPanel.Controls.Add(this.moveBtn);
            this.palletPanel.Controls.Add(this.penToolBtn);
            this.palletPanel.Controls.Add(this.btn9);
            this.palletPanel.Controls.Add(this.btnColor);
            this.palletPanel.Controls.Add(this.label4);
            this.palletPanel.Controls.Add(this.btnAddLayer);
            this.palletPanel.Controls.Add(this.label3);
            this.palletPanel.Controls.Add(this.label2);
            this.palletPanel.Controls.Add(this.label1);
            this.palletPanel.Controls.Add(this.btnSetSize);
            this.palletPanel.Controls.Add(this.textPicHeight);
            this.palletPanel.Controls.Add(this.textPicWidth);
            this.palletPanel.Controls.Add(this.btnLoad);
            this.palletPanel.Controls.Add(this.btnSave);
            this.palletPanel.Controls.Add(this.btnClear);
            this.palletPanel.Controls.Add(this.cmbWidth);
            this.palletPanel.Controls.Add(this.btn8);
            this.palletPanel.Controls.Add(this.btn6);
            this.palletPanel.Controls.Add(this.btn5);
            this.palletPanel.Controls.Add(this.btn4);
            this.palletPanel.Controls.Add(this.btn3);
            this.palletPanel.Controls.Add(this.btn2);
            this.palletPanel.Controls.Add(this.btn1);
            this.palletPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.palletPanel.Location = new System.Drawing.Point(0, 0);
            this.palletPanel.Name = "palletPanel";
            this.palletPanel.Size = new System.Drawing.Size(1062, 94);
            this.palletPanel.TabIndex = 0;
            // 
            // moveBtn
            // 
            this.moveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.moveBtn.Location = new System.Drawing.Point(136, 56);
            this.moveBtn.Name = "moveBtn";
            this.moveBtn.Size = new System.Drawing.Size(120, 30);
            this.moveBtn.TabIndex = 25;
            this.moveBtn.Text = "移動";
            this.moveBtn.UseVisualStyleBackColor = false;
            this.moveBtn.Click += new System.EventHandler(this.moveBtn_Click);
            // 
            // penToolBtn
            // 
            this.penToolBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.penToolBtn.Location = new System.Drawing.Point(12, 56);
            this.penToolBtn.Name = "penToolBtn";
            this.penToolBtn.Size = new System.Drawing.Size(120, 30);
            this.penToolBtn.TabIndex = 24;
            this.penToolBtn.Text = "ペン";
            this.penToolBtn.UseVisualStyleBackColor = false;
            this.penToolBtn.Click += new System.EventHandler(this.penToolBtn_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.Black;
            this.btn9.Location = new System.Drawing.Point(347, 24);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(20, 20);
            this.btn9.TabIndex = 22;
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.Black;
            this.btnColor.Font = new System.Drawing.Font("MS UI Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnColor.ForeColor = System.Drawing.Color.White;
            this.btnColor.Location = new System.Drawing.Point(432, 9);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(93, 34);
            this.btnColor.TabIndex = 21;
            this.btnColor.Text = "現在の色";
            this.btnColor.UseVisualStyleBackColor = false;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(248, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 18);
            this.label4.TabIndex = 20;
            this.label4.Text = "カラー変更";
            // 
            // btnAddLayer
            // 
            this.btnAddLayer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAddLayer.Location = new System.Drawing.Point(262, 56);
            this.btnAddLayer.Name = "btnAddLayer";
            this.btnAddLayer.Size = new System.Drawing.Size(120, 30);
            this.btnAddLayer.TabIndex = 15;
            this.btnAddLayer.Text = "レイヤー追加";
            this.btnAddLayer.UseVisualStyleBackColor = false;
            this.btnAddLayer.Click += new System.EventHandler(this.btnAddLayer_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(693, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 18);
            this.label3.TabIndex = 19;
            this.label3.Text = "高さ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(585, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 18);
            this.label2.TabIndex = 18;
            this.label2.Text = "幅";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 18);
            this.label1.TabIndex = 17;
            this.label1.Text = "ペンサイズ";
            // 
            // btnSetSize
            // 
            this.btnSetSize.Location = new System.Drawing.Point(812, 8);
            this.btnSetSize.Name = "btnSetSize";
            this.btnSetSize.Size = new System.Drawing.Size(118, 32);
            this.btnSetSize.TabIndex = 14;
            this.btnSetSize.Text = "サイズ変更";
            this.btnSetSize.UseVisualStyleBackColor = true;
            this.btnSetSize.Click += new System.EventHandler(this.btnSetSize_Click);
            // 
            // textPicHeight
            // 
            this.textPicHeight.Location = new System.Drawing.Point(737, 10);
            this.textPicHeight.Name = "textPicHeight";
            this.textPicHeight.Size = new System.Drawing.Size(71, 25);
            this.textPicHeight.TabIndex = 13;
            // 
            // textPicWidth
            // 
            this.textPicWidth.Location = new System.Drawing.Point(618, 9);
            this.textPicWidth.Name = "textPicWidth";
            this.textPicWidth.Size = new System.Drawing.Size(71, 25);
            this.textPicWidth.TabIndex = 12;
            // 
            // btnLoad
            // 
            this.btnLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnLoad.Location = new System.Drawing.Point(388, 56);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(120, 30);
            this.btnLoad.TabIndex = 11;
            this.btnLoad.Text = "読み込み";
            this.btnLoad.UseVisualStyleBackColor = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSave.Location = new System.Drawing.Point(640, 56);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClear.Location = new System.Drawing.Point(514, 56);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 30);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "クリア";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cmbWidth
            // 
            this.cmbWidth.FormattingEnabled = true;
            this.cmbWidth.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "8",
            "16",
            "32"});
            this.cmbWidth.Location = new System.Drawing.Point(110, 10);
            this.cmbWidth.Name = "cmbWidth";
            this.cmbWidth.Size = new System.Drawing.Size(121, 26);
            this.cmbWidth.TabIndex = 8;
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.White;
            this.btn8.Location = new System.Drawing.Point(347, 4);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(20, 20);
            this.btn8.TabIndex = 7;
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn6.Location = new System.Drawing.Point(403, 24);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(20, 20);
            this.btn6.TabIndex = 5;
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn5.Location = new System.Drawing.Point(383, 4);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(20, 20);
            this.btn5.TabIndex = 4;
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.Teal;
            this.btn4.Location = new System.Drawing.Point(403, 4);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(20, 20);
            this.btn4.TabIndex = 3;
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.Lime;
            this.btn3.Location = new System.Drawing.Point(365, 4);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(20, 20);
            this.btn3.TabIndex = 2;
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.Red;
            this.btn2.Location = new System.Drawing.Point(367, 24);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(20, 20);
            this.btn2.TabIndex = 1;
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btn1.Location = new System.Drawing.Point(383, 24);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(20, 20);
            this.btn1.TabIndex = 0;
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn_Click);
            // 
            // layerWrapper
            // 
            this.layerWrapper.AutoScroll = true;
            this.layerWrapper.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.layerWrapper.Location = new System.Drawing.Point(120, 96);
            this.layerWrapper.Margin = new System.Windows.Forms.Padding(0);
            this.layerWrapper.Name = "layerWrapper";
            this.layerWrapper.Size = new System.Drawing.Size(788, 354);
            this.layerWrapper.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 450);
            this.Controls.Add(this.layerWrapper);
            this.Controls.Add(this.palletPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.palletPanel.ResumeLayout(false);
            this.palletPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel palletPanel;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Panel layerWrapper;
        private System.Windows.Forms.ComboBox cmbWidth;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox textPicHeight;
        private System.Windows.Forms.TextBox textPicWidth;
        private System.Windows.Forms.Button btnSetSize;
        private System.Windows.Forms.Button btnAddLayer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button penToolBtn;
        private System.Windows.Forms.Button moveBtn;
    }
}

