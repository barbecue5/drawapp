﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;


namespace DrawApp
{
    partial class Form1
    {
        bool mouseDownFlg = false;
        bool _thumbnailDragFlg = false;  //true:描画中
        Point _thumbnailOldLocation = new Point();
        int _thumbnailBeforeIndex = -1;
        int _targetBitmapIndex = -1;
        int _thumbnailAfterIndex = -1;

        private void ThumbnailPic_MouseDown(object sender, MouseEventArgs e)
        {
            Console.WriteLine("ThumbnailPic_MouseDown");
            _thumbnailOldLocation = e.Location;

            ThumbnailPictureBox thumbnailPictureBox = (ThumbnailPictureBox)sender;
            _thumbnailBeforeIndex = this._thumbnailPanel.Controls.IndexOf(thumbnailPictureBox);
            _targetBitmapIndex = this._thumbnailPanel.Controls.IndexOf(thumbnailPictureBox);

            Console.WriteLine("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
            Console.WriteLine("yy" + _thumbnailBeforeIndex);

            //描画中にする
            _thumbnailDragFlg = true;
            //layerPic_MouseMove(sender, e);
        }



        private void ThumbnailPic_MouseEnter(object sender, EventArgs e)
        {

            Console.WriteLine("ThumbnailPic_MouseEnter");

        }



        private void ThumbnailPic_MouseMove(object sender, MouseEventArgs e)
        {
            //サムネイルをドラッグ中でない場合、処理抜け
            if (_thumbnailDragFlg == false) return;
 
            ThumbnailPictureBox thumbnailPictureBox = ((ThumbnailPictureBox)sender);
            int index = this._thumbnailPanel.Controls.IndexOf(thumbnailPictureBox);



            int diff = 0;
            if (e.Y < 0)
            {
                diff = (e.Y - Form1.THUMBNAIL_HEIGHT) / Form1.THUMBNAIL_HEIGHT;
            }
            else
            {
                diff = e.Y / Form1.THUMBNAIL_HEIGHT;
            }
            //if (e.Y < _thumbnailBeforeIndex* Form1.THUMBNAIL_HEIGHT && _thumbnailBeforeIndex != 0)
            //{
            //    diff = -1;
            //}
            //else if (e.Y > Form1.THUMBNAIL_HEIGHT + _thumbnailBeforeIndex * Form1.THUMBNAIL_HEIGHT && _thumbnailBeforeIndex != this._thumbnailPanel.Controls.Count - 1)
            //{
            //    diff = 1;
            //}

            Console.WriteLine("yyyyyyyyyyyyyy" + index);
            Console.WriteLine("e.Y " + e.Y);
            Console.WriteLine("diff " + diff);

            int newIndex = _thumbnailBeforeIndex + diff;
            if (newIndex < 0) newIndex = 0;
            if (newIndex > _thumbnailPanel.Controls.Count - 1) newIndex = _thumbnailPanel.Controls.Count - 1;

            if (_thumbnailBeforeIndex != newIndex)
            {
                Console.WriteLine("%%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% %%%%%%%%%%%%%% ");
                Console.WriteLine("%%%%%%%%%%%%%%  " + e.Y);
                Console.WriteLine("_thumbnailPanel.Controls.Count = " + _thumbnailPanel.Controls.Count);
                Console.WriteLine("layerContainer.Controls.Count = " + this._layerContainerPanel.Controls.Count);
                Console.WriteLine("newIndex = " + newIndex);
                Console.WriteLine("_thumbnailBeforeIndex = " + _thumbnailBeforeIndex);
                //this._thumbnailPanel.Controls.SetChildIndex(thumbnailPictureBox, newIndex);
                //this._thumbnailPanel.ResumeLayout();
                //this._thumbnailPanel.SortChildren();

                SortLayerList(_targetBitmapIndex, newIndex);
                _targetBitmapIndex = newIndex;

                //this._layerContainerPanel.Controls.SetChildIndex(
                //     this._layerContainerPanel.Controls[index], newIndex);
                //this._layerContainerPanel.SortChildren();
                //this._layerContainerPanel.ResumeLayout();
            }

        }

        private void SortLayerList(int index, int newIndex)
        {
            index = layerList.Count - 1 - index;
            newIndex = layerList.Count - 1 - newIndex;
            Bitmap b1 = layerList[index].BitMap;
            int abs = Math.Abs(newIndex - index);
            int sign = Math.Sign(newIndex - index);
            for (int i = 0; i < abs; i++)
            {
                int k = index + i * sign;
                int g = index + (i + 1) * sign;
                Bitmap b2 = layerList[g].BitMap;
                layerList[k].BitMap = b2;
            }


            layerList[newIndex].BitMap = b1;

            //for (int i = 0; i < _layerContainerPanel.Controls.Count; i++)
            //{
            //    Console.WriteLine("_layerContainerPanel.Controls.Count  " + _layerContainerPanel.Controls.Count);
            //    _layerContainerPanel.Controls.RemoveAt(i);
            //}

            //for (int i = 0; i < layerList.Count; i++)
            //{
            //    for (int k = 0; k < layerList[i].Controls.Count; k++)
            //    {
            //        Console.WriteLine("layerList[i].Controls.Count" + layerList[i].Controls.Count);
            //        layerList[i].Controls.RemoveAt(k);
            //    }
            //}

            //List<Bitmap> bitMapList = new List<Bitmap>();
            //for (int i = 0; i < layerList.Count; i++)
            //{
            //    bitMapList.Add(layerList[i].BitMap);
            //}

            //layerList.Clear();
            //for (int i = 0; i < bitMapList.Count; i++)
            //{
            //    LayerPictureBox newLayerPictureBox = MakeNewLayer();
            //    newLayerPictureBox.BitMap = bitMapList[i];
            //}

            //_layerContainerPanel.Controls.Add(layerList[1]);

            //for (int i = 0; i < layerList.Count; i++)
            //{
            //    layerList[i].Parent = null;
            //    _layerContainerPanel.Controls.Add(layerList[i]);
            //    if (i == 0)
            //    {
            //        //layerList[i].Parent = _layerContainerPanel;
            //    }
            //    else
            //    {
            //        layerList[i].Parent = layerList[i - 1].Parent;
            //    }


            //}



            //for (int i = 0; i < layerList.Count; i++)
            //{
            //    Console.WriteLine("tttttttttttttt" + layerList[i].t);
            //    layerList[i].BringToFront();
            //}

            //this._layerContainerPanel.ResumeLayout();
        }

        private void ThumbnailPic_MouseUp(object sender, MouseEventArgs e)
        {

            //描画中を解除
            _thumbnailDragFlg = false;
        }


        /// <summary>
        /// ボタンの上でマウスが押された時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDrag_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseDownFlg = true;
            }
        }

        /// <summary>
        /// ボタンの上でマウスが移動された時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDrag_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDownFlg)
            {
                ThumbnailPictureBox btnDrag = (ThumbnailPictureBox)sender;
                // ドラッグドロップイベントの開始
                btnDrag.DoDragDrop(sender, DragDropEffects.All);
                mouseDownFlg = false;
            }
        }

        /// <summary>
        /// ドラッグ開始時に発生するイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDrag_DragEnter(object sender, DragEventArgs e)
        {
            // 移動エフェクトを設定
            e.Effect = DragDropEffects.Move;
        }

        /// <summary>
        /// ドラッグドロップ操作中にマウスが移動された時に発生するイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDrag_DragOver(object sender, DragEventArgs e)
        {
            Console.WriteLine("btnDrag_DragOver");
            ThumbnailPictureBox btnDrag = (ThumbnailPictureBox)sender;
            // マウスカーソルの位置にボタンを配置する
            btnDrag.Location = PointToClient(new Point(e.X - btnDrag.Width / 2, e.Y - btnDrag.Height / 2));
        }

        /// <summary>
        /// ドラッグドロップした時に発生するイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDrag_DragDrop(object sender, DragEventArgs e)
        {
            // 初期エフェクトを設定
            e.Effect = DragDropEffects.None;
        }


    }
}
