﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawApp
{
    public partial class LayerContainerPanel : Control
    {
        public LayerContainerPanel()
        {
            InitializeComponent();
            //this.BackColor = Color.Transparent;
            this.ControlAdded += new ControlEventHandler(this.SelfControlAdded);
        }

        private void SelfControlAdded(object sender, ControlEventArgs e)
        {
            Console.WriteLine("SelfControlAdded");

        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        public void SortChildren()
        {
            //使えない
            for (int i = 0; i < this.Controls.Count; i++)
            {
                this.Controls[i].BringToFront();
            }
        }
    }
}
