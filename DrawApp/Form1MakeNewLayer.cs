﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;

namespace DrawApp
{
    partial class Form1
    {

        private LayerPictureBox MakeNewLayer(int weight, int height)
        {

            Console.WriteLine("MakeNewLayer");
            LayerPictureBox newLayer = new LayerPictureBox(weight, height);
            newLayer.Location = new Point(0, 0);
            _layerContainerPanel.Controls.Add(newLayer);
            Console.WriteLine(_layerContainerPanel.Controls.Count);

            if (layerList.LastOrDefault() != null)
            {
                layerList.LastOrDefault().Controls.Add(newLayer);
                //newLayer.Parent = layerList.LastOrDefault(); // listが空ならNULLを格納
            }
            else
            {
                _layerContainerPanel.Controls.Add(newLayer);
                //newLayer.Parent = _layerContainerPanel;
            }
            newLayer.BringToFront();
            layerList.Add(newLayer);
            //_selectedLayerNum = layerList.Count - 1;
            _selectedLayerNum = 0;

            newLayer.MouseDown
                += new MouseEventHandler(this.layerPic_MouseDown);

            newLayer.MouseMove
               += new MouseEventHandler(this.layerPic_MouseMove);

            newLayer.MouseUp
              += new MouseEventHandler(this.layerPic_MouseUp);

            newLayer.Paint
                += new PaintEventHandler(this.pic_Resize);

            //newLayer.Resize
            //    += new EventHandler(newLayer.LayerResize);



            newLayer.Thumbnail.MouseDown
                 += new MouseEventHandler(this.ThumbnailPic_MouseDown);

            //newLayer.Thumbnail.MouseEnter
            //      += new EventHandler(this.ThumbnailPic_MouseEnter);

            newLayer.Thumbnail.MouseMove
               += new MouseEventHandler(this.ThumbnailPic_MouseMove);

            newLayer.Thumbnail.MouseUp
              += new MouseEventHandler(this.ThumbnailPic_MouseUp);

            //newLayer.Thumbnail.AllowDrop = true; // 必ず追加する必要があります。
            //newLayer.Thumbnail.MouseDown += btnDrag_MouseDown;
            //newLayer.Thumbnail.MouseMove += btnDrag_MouseMove;
            //newLayer.Thumbnail.DragEnter += btnDrag_DragEnter;
            //newLayer.Thumbnail.DragOver += btnDrag_DragOver;
            //newLayer.Thumbnail.DragDrop += btnDrag_DragDrop;

            this._thumbnailPanel.Controls.Add(newLayer.Thumbnail);
            newLayer.Thumbnail.BringToFront();

            this._thumbnailPanel.SortChildren();
            

            return newLayer;
        }


    }
}
