﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawApp
{
    public partial class LayerPictureBox : PictureBox
    {
        private static int num = 0;
        public int t;
        private Bitmap _bitMap;
        private ThumbnailPictureBox _thumbnail;
        private CheckedListBox _check;

        public Bitmap BitMap
        {
            get => _bitMap;
            set
            {
                this.Image = value;
                this._thumbnail.BitMap = value;
                _bitMap = value;
            }
        }

        public ThumbnailPictureBox Thumbnail {
            get => _thumbnail;
            set => _thumbnail = value;
        }

        public LayerPictureBox(int width, int height)
        {
            InitializeComponent();
            t = num++;
            Console.WriteLine("LayerPictureBox　コンストラクタ");
            this.BackColor = Color.Transparent;
            this.Location = new Point(0, 0);

            this.Width = width;
            this.Height = height;

            this._bitMap = new Bitmap(width, height);
            this.Image = this._bitMap;
            this.BackColor = Color.Transparent;
            this.BorderStyle = BorderStyle.FixedSingle;

            this._thumbnail = new ThumbnailPictureBox(Form1.THUMBNAIL_WIDTH, Form1.THUMBNAIL_HEIGHT);
            this._thumbnail.Parent = this;
            this._thumbnail.BitMap = this._bitMap;

            _check = new CheckedListBox();
            _check.BackColor = Color.Red;
            _check.Items.Add("表示", true);
            //_check.Width = _check.GetItemHeight(0);
            //_check.Height = _check.GetItemHeight(0);
            _check.Width = 20;
            _check.Height = 20;
            this._thumbnail.Controls.Add(_check);

            _check.Click += new EventHandler(Disp);

        }

        private void Disp(object sender, EventArgs e)
        {
            Console.WriteLine(_check.GetItemChecked(0));
            if (_check.GetItemChecked(0))
            {
                Bitmap newBitmap = new Bitmap(this.Width, this.Height);
                this.Image = newBitmap;
            }
            else
            {
                this.Image = this.BitMap;
            }
        }

        public void PaintThumbnail()
        {
            this._thumbnail.ChangeBitmap(this._bitMap);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            Console.WriteLine("LayerPictureBox　OnPaint 発動");
            base.OnPaint(pe);
        }

        public void remove()
        {
            //this.Parent.Controls.Remove(this);
        }

        // 主にリサイズされたときに明示的に呼ぶ
        public void NewBitmap()
        {
            NewBitmap(0, 0);
        }
        // 主にリサイズされたときに明示的に呼ぶ
        public void NewBitmap(int x,int y)
        {
            Console.WriteLine(this.Width);
            Console.WriteLine(this.Height);
            Bitmap newBitmap = new Bitmap(this.Width, this.Height);
            //左上基準でnewBitmapへ_bitmapを複製
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                g.DrawImage(this.BitMap, new Point(x, y));
            }
            this.BitMap = newBitmap;//newBitmapを新サイズで更新
            //this.BackColor = Color.Bisque;
        }

        public void SetBitMap(Bitmap bm)
        {
            this.BitMap = bm;
        }

        internal void LayerResize(object sender, EventArgs e)
        {
            // widthとheightを変更したら2回呼ばれる可能性がある
            NewBitmap();
        }

        public void Fill(int x, int y, Color _selectedColor)
        {
            Color ableColor = this.BitMap.GetPixel(x,y);
            FillLoop(x, y, _selectedColor, ableColor);
        }

        private async void test()
        {
            await Task.Delay(1000);
        }
        int count;
        //★内部を塗りつぶす
        public void FillLoop(int x, int y, Color selectedColor, Color ableColor)
        {
            if(count > 100)
            {
                //test();
                //System.Threading.Thread.Sleep(10);
                count = 0;
            }
            count++;
            Color color = this.BitMap.GetPixel(x, y);

            if (color != ableColor) return;
            this.BitMap.SetPixel(x, y, selectedColor);
            if(x + 1 < this.BitMap.Width) FillLoop(x + 1, y, selectedColor, ableColor);
            if (x - 1 > 0) FillLoop(x - 1, y, selectedColor, ableColor);
            if (y + 1 < this.BitMap.Height) FillLoop(x, y + 1, selectedColor, ableColor);
            if (y - 1 > 0) FillLoop(x, y - 1, selectedColor, ableColor);
        }

    }
}
