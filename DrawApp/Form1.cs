﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;

namespace DrawApp
{


    public partial class Form1 : Form
    {
        //git push 2019/02/28 テストプッシュ
        //git push 2019/02/06 塗りつぶし追加
        //git push 2019/02/06 保存ダイアログ追加
        //git push 2019/02/04
        //git push テスト

        public static int form1_width = 800;
        public static int form1_height = 500;
        public static int pallet_panel_width = form1_width;
        public static readonly int PALLET_PANEL_HEIGHT = 65;
        public static readonly int PALLET_PANEL_X = 0;
        public static readonly int PALLET_PANEL_Y = 0;

        
        public static readonly int THUMBNAIL_PANEL_WIDTH = 100;
        public static readonly int THUMBNAIL_PANEL_HEIGHT = form1_height - PALLET_PANEL_HEIGHT;
        public static readonly int THUMBNAIL_PANEL_X = 0;
        public static readonly int THUMBNAIL_PANEL_Y = PALLET_PANEL_HEIGHT;

        public static readonly int THUMBNAIL_WIDTH = 70;
        public static readonly int THUMBNAIL_HEIGHT = 50;

        public static readonly int LAYERWRAPPER_PANEL_WIDTH = form1_width - THUMBNAIL_PANEL_WIDTH;
        public static readonly int LAYERWRAPPER_PANEL_HEIGHT = form1_height - PALLET_PANEL_HEIGHT;
        public static readonly int LAYERWRAPPER_PANEL_X = THUMBNAIL_PANEL_WIDTH;
        public static readonly int LAYERWRAPPER_PANEL_Y = PALLET_PANEL_HEIGHT;

        public static readonly int LAYERCONTAINER_PANEL_WIDTH = 400;
        public static readonly int LAYERCONTAINER_PANEL_HEIGHT = 300;
        public static readonly int LAYERCONTAINER_PANEL_X = 50;
        public static readonly int LAYERCONTAINER_PANEL_Y = 50;
        Bitmap _bitmap = null;//インスタンス変数を作成
        //int _layerPicWidth;
        //int _layerPicHeight;
        int _selectedLayerNum;
        List<LayerPictureBox> layerList;
        private ThumbnailPanel _thumbnailPanel;
        private LayerContainerPanel _layerContainerPanel;
        //List<ThumbnailPictureBox> thumbnailPictureList;
        bool drawFlg = false;  //true:描画中
        Point oldLocation = new Point();
        Color _selectedColor = Color.Black;//ペン色初期値
        private int _toolMode; // 0:普通のペン　1:塗りつぶしツール

        public Form1()
        {
            InitializeComponent();
            _selectedLayerNum = 0;
            layerList = new List<LayerPictureBox>();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _toolMode = 0;
            Console.WriteLine("COUNT " + palletPanel.Controls.Count);
            _selectedColor = btnColor.BackColor;
            //Console.WriteLine("COUNT " + layerWrapper.Controls.Count);
            this.Width = form1_width;
            this.Height= form1_height;
            this.Left = 200;
            this.Top = 100;
            palletPanel.Width = pallet_panel_width;
            palletPanel.Height = PALLET_PANEL_HEIGHT;
            palletPanel.Left = PALLET_PANEL_X;
            palletPanel.Top = PALLET_PANEL_Y;
            this._thumbnailPanel = new ThumbnailPanel();
            this._thumbnailPanel.Width = Form1.THUMBNAIL_PANEL_WIDTH;
            this._thumbnailPanel.Height = Form1.THUMBNAIL_PANEL_HEIGHT;
            this._thumbnailPanel.Location = new Point(Form1.THUMBNAIL_PANEL_X, Form1.THUMBNAIL_PANEL_Y);
            this._thumbnailPanel.BackColor = Color.Brown;
            this._thumbnailPanel.AutoScroll = true;
            this.Controls.Add(this._thumbnailPanel);

            layerWrapper.Width = Form1.LAYERWRAPPER_PANEL_WIDTH;
            if (this.ClientSize.Width < this.DisplayRectangle.Size.Width)
            {
                layerWrapper.Width -= SystemInformation.VerticalScrollBarWidth;
            }
            layerWrapper.Location = new Point(Form1.LAYERWRAPPER_PANEL_X, Form1.LAYERWRAPPER_PANEL_Y);

            layerWrapper.Height = Form1.LAYERWRAPPER_PANEL_HEIGHT;
            //layerWrapper.BackColor = Color.FromArgb(180, 180, 180);

            this._layerContainerPanel = new LayerContainerPanel();
            this._layerContainerPanel.Width = Form1.LAYERCONTAINER_PANEL_WIDTH;
            this._layerContainerPanel.Height = Form1.LAYERCONTAINER_PANEL_HEIGHT;
            this._layerContainerPanel.Location = new Point(Form1.LAYERCONTAINER_PANEL_X, Form1.LAYERCONTAINER_PANEL_Y);
            this._layerContainerPanel.BackColor = Color.White;
            this.layerWrapper.Controls.Add(this._layerContainerPanel);

            LayerPictureBox newLayer = MakeNewLayer();


            //ペンの太さComboBox(cmbWidth)の初期選択項目を設定
            cmbWidth.SelectedIndex = 2;

            Form1_Resize(sender, e);

            ChangeSelectBtn(penToolBtn);

        }



        private void layerPic_MouseDown(object sender, MouseEventArgs e)
        {
            oldLocation = e.Location;
            drawFlg = true;
            switch (_toolMode)
            {
                case 0:
                    Console.WriteLine("pen mode layerPic_MouseDown");
                    //描画中にする
                     layerPic_MouseMove(sender, e);
                    break;
                case 1:
                    Console.WriteLine("fill mode layerPic_MouseDown");
                    LayerPictureBox layer = (LayerPictureBox)sender;
                    layer.Fill(e.X, e.Y, _selectedColor);
                    break;
                case 2:
                    Console.WriteLine(e.X + e.Y);

                    break;
            }
        }

        private void layerPic_MouseMove(object sender, MouseEventArgs e)
        {
            switch (_toolMode)
            {
                case 0:
                    //Console.WriteLine("e.Y + e.X" + e.Y + " + "+ e.X);
                    //Console.WriteLine("layerPic_MouseMove");
                    //描画中でない場合、処理抜け
                    if (drawFlg == false) return;
                    Bitmap nowBitmap = layerList.Last().BitMap;

                    using (Graphics g = Graphics.FromImage(nowBitmap))
                    {
                        //描画する(前回位置から新位置へ線を描く)
                        Draw(g, oldLocation, e.Location);
                        //Draw2(g, e.Location);
                    }
                    layerList.Last().Image = nowBitmap;

                    //新位置を保存
                    oldLocation = e.Location;
                    break;
                case 2:
                    if (drawFlg == false) return;
                    int newX = e.X - oldLocation.X;
                    int newY = e.Y - oldLocation.Y;
                    if(Math.Abs(newX) >10 || Math.Abs(newY) > 10)
                    {
                        layerList.Last().NewBitmap(newX, newY);
                        oldLocation = e.Location;
                    }
                    Console.WriteLine("x " + newX + "  :   y " + newY);

                    break;
            }
        }

        private void layerPic_MouseUp(object sender, MouseEventArgs e)
        {
            layerList.Last().PaintThumbnail();
            //描画中を解除
            drawFlg = false;
        }


        //全ての色ボタンクリックイベント
        private void btn_Click(object sender, EventArgs e)
        {
            //イベント元オブジェクトをButtonでキャストしてBackColorを取得
            _selectedColor = ((Button)sender).BackColor;
            btnColor.BackColor = _selectedColor;
        }




        private void btnClear_Click(object sender, EventArgs e)
        {
            //メッセージボックスを表示する
            DialogResult result = MessageBox.Show("全てのレイヤーが消えますがよろしいですか？",
                "質問",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);

            //何が選択されたか調べる
            if (result == DialogResult.OK)
            {
                layerAllClear();
                MakeNewLayer();
                Form1_Resize_Logic();
                //「はい」が選択された時
                Console.WriteLine("「はい」が選択されました");
            }
            else if (result == DialogResult.No)
            {
                //「いいえ」が選択された時
                Console.WriteLine("「いいえ」が選択されました");
            }
            else if (result == DialogResult.Cancel)
            {
                //「キャンセル」が選択された時
                Console.WriteLine("「キャンセル」が選択されました");
            }


        }

        private void clear()
        {
            using (Graphics g = Graphics.FromImage(_bitmap))
            {
                //描画する
                g.FillRectangle(Brushes.White, new Rectangle(0, 0, _bitmap.Width, _bitmap.Height));
            }
            layerList.Last().Image = _bitmap;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            //SaveFileDialogクラスのインスタンスを作成
            SaveFileDialog sfd = new SaveFileDialog();

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            sfd.FileName = "新しい画像.png";
            //はじめに表示されるフォルダを指定する
            sfd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            sfd.Filter = "pngファイル(*.png;*.htm)|*.html;*.htm|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに選択されるものを指定する
            //2番目の「すべてのファイル」が選択されているようにする
            sfd.FilterIndex = 2;
            //タイトルを設定する
            sfd.Title = "保存先のファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            sfd.RestoreDirectory = true;
            //既に存在するファイル名を指定したとき警告する
            //デフォルトでTrueなので指定する必要はない
            sfd.OverwritePrompt = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            sfd.CheckPathExists = true;

            //ダイアログを表示する
            if (sfd.ShowDialog() == DialogResult.OK)
            {




                Bitmap saveBitmap = new Bitmap(_layerContainerPanel.Width, _layerContainerPanel.Height);

                for (int i = 0; i < layerList.Count; i++)
                {
                    using (Graphics g = Graphics.FromImage(saveBitmap))
                    {
                        g.DrawImage(layerList[i].BitMap, new Point(0, 0));
                    }

                }
                saveBitmap.Save(sfd.FileName, ImageFormat.Png);
                //OKボタンがクリックされたとき、選択されたファイル名を表示する
                Console.WriteLine(sfd.FileName);
            }

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            //OpenFileDialogクラスのインスタンスを作成
            OpenFileDialog ofd = new OpenFileDialog();

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            ofd.FileName = "default.html";
            //はじめに表示されるフォルダを指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            ofd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しないとすべてのファイルが表示される
            ofd.Filter = "HTMLファイル(*.html;*.htm)|*.html;*.htm|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに選択されるものを指定する
            //2番目の「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 2;
            //タイトルを設定する
            ofd.Title = "開くファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
            //存在しないファイルの名前が指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckFileExists = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            //ダイアログを表示する
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //OKボタンがクリックされたとき、選択されたファイル名を表示する
                Console.WriteLine(ofd.FileName);
                layerAllClear();
                LayerPictureBox layerPictureBox = MakeNewLayer();
                Image img = Bitmap.FromFile(ofd.FileName);
                Bitmap bitmap = new Bitmap(img);

                _layerContainerPanel.Width = bitmap.Width;
                _layerContainerPanel.Height = bitmap.Height;

                layerPictureBox.BitMap = bitmap;
                layerPictureBox.Width = bitmap.Width;
                layerPictureBox.Height = bitmap.Height;

                img.Dispose();

                Form1_Resize_Logic();
            }


        }

        private void layerAllClear()
        {
            //var notNullLayerList = layerList.Where(n => n != null);
            for (int i =0;i< layerList.Count; i++)
            {
                if (layerList[i] == null) continue;
                this._thumbnailPanel.Controls.Remove(layerList[i].Thumbnail);
                _layerContainerPanel.Controls.Remove(layerList[i]);
                layerList[i] = null;
            }
            layerList.Clear();
        }


        // 配置コマンド用メソッド
        private void btnPut_Click(object sender, EventArgs e)
        {
            LayerPictureBox layerPictureBox = MakeNewLayer();
            Image img = Bitmap.FromFile("test.png");
            Bitmap bitmap = new Bitmap(img);
            layerPictureBox.BitMap = bitmap;

            if (_layerContainerPanel.Width < bitmap.Width)
            {
                _layerContainerPanel.Width = bitmap.Width;
            }


            if (_layerContainerPanel.Height < bitmap.Height)
            {
                _layerContainerPanel.Height = bitmap.Height;
            }

            img.Dispose();
        }

        private void pic_Resize(object sender, EventArgs e)
        {
            textPicWidth.Text = _layerContainerPanel.Width.ToString();
            textPicHeight.Text = _layerContainerPanel.Height.ToString();
        }

        private void btnSetSize_Click(object sender, EventArgs e)
        {
            //入力サイズの取得
            int width = Int32.Parse(textPicWidth.Text);
            int height = Int32.Parse(textPicHeight.Text);

            //親コンテナのサイズ変更
            _layerContainerPanel.Size = new Size(width, height);
            for(int i = 0; i < layerList.Count; i++)
            {
                layerList[i].Width = width;
                layerList[i].Height = height;
            }
            //layerList.Last().Invalidate();

            //新しいPictureBoxの画像を生成
            layerList.Last().NewBitmap();
            //newBitmapForLayer(_selectedLayerNum);
            Form1_Resize(sender, e);

        }

        private void newBitmapForLayer(int num)
        {
            //新しくbitmapを作るレイヤーを選択
            LayerPictureBox targetLayer = layerList[num];

            Bitmap newBitmap = new Bitmap(_layerContainerPanel.Width, _layerContainerPanel.Height);
            //左上基準でnewBitmapへ_bitmapを複製
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                g.DrawImage(targetLayer.BitMap, new Point(0, 0));
            }
            targetLayer.BitMap = newBitmap;//newBitmapを新サイズで更新
                                           //Imageへの割り当て直しは.BitMapで自動的に行う
        }


        private LayerPictureBox MakeNewLayer()
        {
            return
                MakeNewLayer(
                    this._layerContainerPanel.Width,
                    this._layerContainerPanel.Height
                );
        }


        private void btnAddLayer_Click(object sender, EventArgs e)
        {
            MakeNewLayer();
            penToolBtn_Click(penToolBtn, e);
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                btnColor.BackColor = colorDialog.Color;
                _selectedColor = colorDialog.Color;
                
            }
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            Form1_Resize_Logic();
        }

        private void Form1_Resize_Logic()
        {
            Console.WriteLine("Form1_Resize ");
            if (this._thumbnailPanel == null) return;
            palletPanel.Width = this.Width;
            this._thumbnailPanel.Height = this.Height - PALLET_PANEL_HEIGHT;
            this._thumbnailPanel.AutoScroll = true;
            if (layerWrapper.ClientSize.Height < layerWrapper.DisplayRectangle.Size.Height)
            {
                Console.WriteLine("通った！！！！！！！！！！！！！！！");
                layerWrapper.Width = this.Width - _thumbnailPanel.Width;
                layerWrapper.Width = layerWrapper.Width - SystemInformation.VerticalScrollBarWidth;
            }
 
             if (layerWrapper.ClientSize.Width < layerWrapper.DisplayRectangle.Size.Width)
            {
                Console.WriteLine("yyyyyyyyyyyyy 通った！！！！！！！！！！！！！！！");
                layerWrapper.Height = this.Height - PALLET_PANEL_HEIGHT;
                layerWrapper.Height = layerWrapper.Height - SystemInformation.HorizontalScrollBarHeight;
                layerWrapper.Height -= SystemInformation.CaptionHeight;
            }

        }

        private void fillToolBtn_Click(object sender, EventArgs e)
        {
            ChangeSelectBtn((Button)sender);
            _toolMode = 1;
        }

        private void penToolBtn_Click(object sender, EventArgs e)
        {
            ChangeSelectBtn((Button)sender);
            _toolMode = 0;
        }

        private void moveBtn_Click(object sender, EventArgs e)
        {
            ChangeSelectBtn((Button)sender);
            _toolMode = 2;

        }


        Button _beforeButton;
        void ResetBeforeBtn()
        {
            if(_beforeButton != null)
            {
                _beforeButton.BackColor = Color.FromArgb(224, 224, 224);
                _beforeButton.ForeColor = Color.Black;
            }
        }

        void ChangeSelectBtn(Button b)
        {
            ResetBeforeBtn();
            b.BackColor = Color.Red;
            b.ForeColor = Color.White;
            _beforeButton = b;
        }
    }
}
