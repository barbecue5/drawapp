﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawApp
{
    public partial class ThumbnailPictureBox : PictureBox
    {
        private Bitmap _bitMap;
        private int _smallWidth;
        private int _smallHeight;

        public Bitmap BitMap
        {
            get => _bitMap;
            set
            {
                ChangeBitmap(value);
                //this.Image = value;
                //_bitMap = value;
            }
        }

        /* Bitmap指定できるコンストラクタ追加予定        
         public ThumbnailPictureBox(int width, int height,Bitmap bitmap)
                {
                }
         */

        public ThumbnailPictureBox(int width, int height)
        {
            InitializeComponent();
            Console.WriteLine("SmallPictureBox　コンストラクタ");
            this.Width = width;
            this.Height = height;
            this._smallWidth = width;
            this._smallHeight = height;
            this.BackColor = Color.Aqua;
            //this.Location = new Point(0, 0);

            this.Image = new Bitmap(width, height);
            this.BackColor = Color.White;
            this.BorderStyle = BorderStyle.FixedSingle;
        }

        public void ChangeBitmap(Bitmap bitmap)
        {
            Bitmap newBitmap = new Bitmap(this._smallWidth, this._smallHeight);
            Graphics g = Graphics.FromImage(newBitmap);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(bitmap, 0, 0, this._smallWidth, this._smallHeight);
            g.Dispose();
            this.Image = newBitmap;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            Console.WriteLine("SmallPictureBox　OnPaint 発動");
            base.OnPaint(pe);
        }
    }
}
