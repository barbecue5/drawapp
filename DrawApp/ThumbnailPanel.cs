﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawApp
{
    public partial class ThumbnailPanel : Panel
    {
        public ThumbnailPanel()
        {
            InitializeComponent();


            this.ControlAdded += new ControlEventHandler(this.SelfControlAdded);
        }

        private void SelfControlAdded(object sender, ControlEventArgs e)
        {
            //ThumbnailPictureBox thumbnailPictureBox = (ThumbnailPictureBox)sender;
            //thumbnailPictureBox.BringToFront();

            //for (int i = 0; i < this.Controls.Count; i++)
            //{
            //    this.Controls[i].Location = new Point(0, i *60);
            //}
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        internal void SortChildren()
        {
            for (int i = 0; i < this.Controls.Count; i++)
            {
                Console.WriteLine("xxxxxxxx" + i);
                this.Controls[i].Location = new Point(0, i * Form1.THUMBNAIL_HEIGHT);
            }
        }
    }
}
