﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;

namespace DrawApp
{
    partial class Form1
    {

        private void Draw2(Graphics g, Point xy1)
        {
            int penWidth = Int32.Parse(cmbWidth.SelectedItem.ToString());
            Brush brush = new SolidBrush(_selectedColor);
            RectangleF rect = new RectangleF(  //円を作成
               xy1.X - (penWidth / 2),
               xy1.Y - (penWidth / 2),
               penWidth,
               penWidth);
                        g.FillEllipse(brush, rect);//円を描く
        }


        private void Draw(Graphics g, Point xy1, Point xy2)
        {
            int penWidth = Int32.Parse(cmbWidth.SelectedItem.ToString());
            if (penWidth <= 1)
            {
                //1pixelの円は描画できない為、線で描く
                Pen pen = new Pen(_selectedColor, penWidth);
                g.DrawLine(pen, xy1, xy2);
                return;
            }

            Brush brush = new SolidBrush(_selectedColor);


            bool drawX = true; //true :X軸基準で描画
                               //false:Y軸基準で描画

            if (Math.Abs(xy2.X - xy1.X) >
               Math.Abs(xy2.Y - xy1.Y))
            {
                //幅の方が大きい場合 -> X軸で円描画ループ

                //xy2.Xが大きくなるように入れ替え
                if (xy1.X > xy2.X)
                {  //Pointの値交換
                    Point p = xy1;
                    xy1 = xy2;
                    xy2 = p;
                }

                drawX = true;
            }
            else
            {
                //高さの方が大きい場合 -> Y軸で円描画ループ

                //xy2.Yが大きくなるように入れ替え
                if (xy1.Y > xy2.Y)
                {  //Pointの値交換
                    Point p = xy1;
                    xy1 = xy2;
                    xy2 = p;
                }

                drawX = false;
            }

            if (drawX == true)
            {
                //X軸基準で描画
                float y = (float)xy1.Y;

                //傾きの計算 
                //(Xの刻みに対応したYの最小刻み値を計算(Yの差÷Xの差))
                float a =
                  ((float)xy2.Y - xy1.Y) /  //値大point Y - 値小Point Y
                  ((float)xy2.X - xy1.X);   //値大point X - 値小Point X

                for (int x = xy1.X; x <= xy2.X; x++)
                {//X座標が最大値になる迄
                    RectangleF rect = new RectangleF(  //円を作成
                      x - (penWidth / 2),
                      y - (penWidth / 2),
                      penWidth,
                      penWidth);
                    g.FillEllipse(brush, rect);//円を描く

                    y = y + a;  //yの最小刻み値を足す
                }
            }
            else
            {
                //Y軸基準で描画
                float x = (float)xy1.X;

                //傾きの計算 
                //(Yの刻みに対応したXの最小刻み値を計算(Xの差÷Yの差))
                float a =
                  ((float)xy2.X - xy1.X) /  //値大point X - 値小Point X
                  ((float)xy2.Y - xy1.Y);   //値大point Y - 値小Point Y

                for (int y = xy1.Y; y <= xy2.Y; y++)
                {//Y座標が最大値になる迄
                    RectangleF rect = new RectangleF(  //円を作成
                      x - (penWidth / 2),
                      y - (penWidth / 2),
                      penWidth,
                      penWidth);
                    g.FillEllipse(brush, rect);//円を描く

                    x = x + a;  //xの最小刻み値を足す
                }
            }

        }
    }
}
